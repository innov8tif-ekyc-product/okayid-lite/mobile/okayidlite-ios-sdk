Pod::Spec.new do |s|
    s.name             = 'OkayIDLite'
    s.version          = '1.0.12'
    s.license          = { :file => 'LICENSE', :type => 'Commercial' }
    s.author           = { 'Innov8tif' => 'ekyc.team@innov8tif.com' }
    s.homepage         = 'https://innov8tif.com'
    s.summary          = 'OkayIDLite iOS SDK.'
    s.source           = { :git => 'https://gitlab.com/innov8tif-ekyc-product/okayid-lite/mobile/okayidlite-ios-sdk.git', :tag => 'v1.0.12', :branch => 'master'  }
    s.platform = :ios
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.ios.deployment_target = '11.0'
    s.ios.vendored_frameworks = 'OkayIDLite.framework'
    s.swift_version = '5'

    s.dependency 'GoogleMLKit/TextRecognition'
    s.dependency 'CryptoSwift', '~> 1.5.1'
  end